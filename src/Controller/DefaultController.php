<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use App\Entity\Page;

class DefaultController extends Controller
{

    /**
    * @Route("/", name="home")
    **/
    public function index(){
        $pages = $this->getDoctrine()
        ->getRepository(Page::class)
        ->findBy(['draft' => false]);

        return $this->render("default/index.html.twig", [
            'pages' => $pages
        ]);
    }

    /**
    * @Route("/page/{slug}", name="page_show")
    **/
    public function showPage(Page $page){
        return $this->render("default/show.html.twig", [
        'page' => $page
        ]);
    }

}

?>